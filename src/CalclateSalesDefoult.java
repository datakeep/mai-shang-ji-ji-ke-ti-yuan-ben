//こだちくんver.
package jp.alhinc.saito_kana.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales{
	public static void main (String[] args) {

		// コマンドライン引数が渡されていない場合のエラー処理
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		if(!readFile(args[0],branchNames,branchSales,"branch.lst")){
			return;
		}

		// 売上ファイルの読み込み処理
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i <files.length ; i++) {
			String fileName = files[i].getName();
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		// 売上ファイルが連番になっていない場合のエラー処理
		for(int i = 0; i <rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter  - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;

		for(int i = 0; i< rcdFiles.size(); i++){
			try{
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				ArrayList<String> fileContents = new ArrayList<>();
				String line = "";
				while((line = br.readLine()) != null){
					fileContents.add(line);
				}
				String fileName = rcdFiles.get(i).getName();

				// 行数チェック
				if(fileContents.size() !=2) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				// 売上ファイルの売上金額が数字ではない場合のエラー処理
				if(!fileContents.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				// 支店コードが支店定義ファイルに該当しなかった場合
				if(!branchNames.containsKey(fileContents.get(0))) {
					System.out.println(fileName + "支店コードが不正です");
					return;
				}

				String branchCode= fileContents.get(0);

				// 売り上げ金額の加算
				long fileSale = Long.parseLong(fileContents.get(1));
				Long saleAmount = branchSales.get(branchCode) + fileSale;

				// 集計した売上金額が10桁を超えた場合のエラー処理
				if(saleAmount >= 10000000000L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				// branchSalesマップに支店名をキーに売上額を格納
				branchSales.put(branchCode, saleAmount);

			}catch(IOException e) {
				System.out.println("エラーが発生しました。");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e ) {
						System.out.println("closeできませんでした。");
						return;
					}
				}
			}
		}

		if(!writeFile(args[0],branchNames,branchSales,"branch.out")) {
			return;
		}

	}

	private static boolean readFile(String filepath, Map <String,String>branches,Map <String,Long>sales,String filename) {

		// 支店定義ファイル読み込み処理
		BufferedReader br = null;

		try {
			File file = new File(filepath, filename);
			FileReader fr = new FileReader(file);

			// 支店定義ファイルがない場合のエラー処理
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null){
				String[] items = line.split(",");

				// 支店定義ファイルのフォーマットが違う場合のエラー処理
				if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				branches.put(items[0], items[1]);
				sales.put(items[0], 0L);

			}
		}catch(IOException e) {
			System.out.println("エラーが発生しました。");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e ) {
					System.out.println("closeできませんでした。");
					return false;
				}
			}
		}return true;
	}


	private static boolean writeFile(String path, Map<String,String> branches,Map<String,Long> sales,String filename) {

		// 支店別集計ファイルの書き込み処理
		BufferedWriter bw = null;
		try {
			File file = new File(path, filename);
			bw = new BufferedWriter(new FileWriter(file));

			for(String key : branches.keySet()) {
				bw.write(key + "," + branches.get(key) + "," + sales.get(key));
				// 改行
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("エラーが発生しました。");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e ) {
					System.out.println("closeできませんでした。");
					return false;
				}

			}
		}
		return true;
	}
}

/*package jp.alhinc.saito_kana.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
    public static void main(String[] args) {
	    //すべての支店コードとそれに対応する支店名を保持②
    	//2つのマップを用意
    	//Map（支店定義ファイル、売上ファイル）の宣言の形
    	//支店コードも支店名も文字列として扱う
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		if(readFile(args[0], branchNames, branchSales, "branch.lst") == false) {
			return;
		}

		//売上ファイル読み込み処理③
		//売上ファイルは支店定義ファイルと違って複数あるので、ファイル名取得してファイル名で絞る
		//売上ファイルの取得 //すでにファイル型になってるからnew fileにしなくていい
		File file = new File(args[0]);
		File[] files = file.listFiles();

		//コマンドライン引数が渡されていない場合エラーメッセージを表示、処理を終了⑨
		//今回のアプリ仕様はコマンドライン引数でディレクトリを指定するという一つだけだから、argsの要素数が1つ以外だったらエラーにすればいい
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//リストの宣言（リストという新しい箱や表を作るイメージ）
		List<File> rcdFiles = new ArrayList<>();
		//配列の要素取り出すときはlength
		for(int i = 0; i < files.length; i++) {
			//ファイル名の取得
			String fileName = files[i].getName();

			//正規表現内（８桁rcdがつくファイル）の条件に該当したら、
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				//リストにどんどん詰めていく（for文なのでそれを繰り返し）
				//rcdFilesは該当したファイルたちのこと
				rcdFiles.add(files[i]);
			}
		}
        //エラー処理：売上ファイルが連番になっているかチェック⑥
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			//rcdFilesの要素数0番目から要素数-1番目まで繰り返す
			//そうしないとget(i + 1)で要素数外の要素まで数えるという例外が起きてしまうので最後まで繰り返す必要はない
			//ファイル名の数字を取り出すためにまずgetNameメソッドでint型のformerという変数にファイル名取り出す
			//かつ、substringメソッドで取り出したファイル名の0-8番目までの文字を切り取る
			//rcdFilesはリストなのでgetで(i番目)取得
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			//for文で次の要素見るときもおなじ処理を行う//laterという変数に取得
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			//変数laterとformerの差が1でないならば
			if((latter - former) != 1) {
				System.out.println("売上ファイルが連番になっていません");
				//戻り値のないreturn文で終了
				return;
			}
		}

		//支店コードと売上額の抽出、加算④
		//リストの要素数出す時はsize
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				//正規表現の条件に該当したファイルの取得
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				//新たに＜String型＞のリスト（入れ物）を用意
				ArrayList<String> fileContents = new ArrayList<>();
				//String line;	のラインの宣言と同意
				String line = "";
				//0でなければ１行ずつ読む
				while((line = br.readLine()) != null) {

					//読み込んだモノを用意したリストに追加
					fileContents.add(line);
				}

				String fileName = rcdFiles.get(i).getName();

				//行数チェック⑨（支店コードの存在確認よりも前に行数チェック入れないと例外発生しちゃう）
				//fileContentsの要素数が2じゃなければエラー、リストなので要素はsizeで取り出す
				if(fileContents.size() != 2) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				//売上金額数字チェック⑩
				if(!fileContents.get(i).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//支店コードの存在確認⑧
				//branchNamesというMapに支店コードと支店名が入っている
				//fileContents.get(0)はそのMap中の0番目（１行目にある支店コード）を取り出す
				//containsKeyメソッドを使うと引数（）番目のキーがMapに含まれるか判定してくれる
				if(!branchNames.containsKey(fileContents.get(0))) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				//支店コード
				//getなので(0)１行目にある支店コードの取り出し
				String branchCode = fileContents.get(0);

				//売上金額の加算
				//ファイルから取り出したものは文字として読み込まれてしまうのでParseLongメソッドでString型からlong型に変換
				//売上ファイル2行目1000円取得
				//saleAmountには1000円が、branchCodeには支店コードが入ってる、加算の文
				long fileSale = Long.parseLong(fileContents.get(1));
				Long saleAmount = branchSales.get(branchCode) + fileSale;

				//集計した売上金額が10桁を超えた場合エラーメッセージを表示し処理を終了⑦
				//saleAmountには加算された(足し算が終った）直後の売上金額が入っている
				//javaで不通に数字書きこむとint型として扱われるがint型は大きい数字が扱えないのでlong型だよと表すためにLを金額の後ろにつける
				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//Mapに値を追加（キーの支店コード,バリューの金額）
				branchSales.put(branchCode, saleAmount);

			//エラー処理④
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(writeFile(args[0], branchNames, branchSales, "branch.out") == false) {
			return;
		}
    }
    private static boolean writeFile(String totalSale, Map<String, String>branchNames, Map<String, Long>branchSales, String branchout){
        //支店別集計ファイル書き込み処理
        BufferedWriter bw = null;
        try {
        	File file1 =  new File(totalSale,"brnchout");
        	bw = new BufferedWriter(new FileWriter(file1));

        	for(String key : branchNames.keySet()) {
        		bw.write(key +  "," + branchNames.get(key) + "," + branchSales.get(key));
        		bw.newLine();
        	}

        //エラー処理
        }catch(IOException e) {
        	System.out.println("予期せぬエラーが発生しました");
        	return false;
        }finally {
        	if(bw != null) {
        		try {
        			bw.close();
        		}catch(IOException e) {
        			System.out.println("予期せぬエラーが発生しました");
        			return false;
        		}
        	}
        }
        return true;
    }
    private static boolean readFile(String totalSale, Map<String, String>branchNames, Map<String, Long>branchSales, String branchlst){
    	//支店定義ファイル読み込み処理
		BufferedReader br = null;
		try {
			File file = new File(totalSale, branchlst);

			//支店定義ファイルは存在するか
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			br = new BufferedReader(new FileReader(file));

			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);
			}

		//エラー処理
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
    }
}*/
